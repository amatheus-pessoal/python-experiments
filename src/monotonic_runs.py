def monotonic_runs(seq):
    seqiter = iter(seq)
    current_run = [next(seqiter)]
    increasing = None
    for item in seqiter:
        if increasing is None:
            increasing = item > current_run[-1]
        if (increasing and item > current_run[-1]) or (not increasing and item < current_run[-1]):
            current_run.append(item)
        else:
            yield current_run if increasing else current_run[::-1]
            current_run = [item]
            increasing = None
    if current_run:
        yield current_run


def test_monotonic_runs():
    seq = [1, 2, 3, 2, 1, 4, 5, 6, 7]
    expected = [[1, 2, 3], [1, 2], [4, 5, 6, 7]]
    assert list(monotonic_runs(seq)) == expected
